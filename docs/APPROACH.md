### JPT approach

Performance of a web application is a high-level measure of a very complex system.
JPT aims to address the most important challenges in performance testing.

#### Meaningfulness

Performance testing offers many perspectives to take and many interpretations to make.
JPT aims to use the most meaningful ones.

We understand meaningfulness as the ability to drive decisions, which will have real and positive impact on Jira users.

In order for the insights to be meaningful, they need to reflect end-user experience. JPT measures Jira interactions
via a real browser. System orchestration imitates a Jira admin. E.g. it uses `start-jira.sh`, Support-recommended
customizations like `setenv.sh` and works over SSH.

#### Robustness

Performance testing involves multiple moving parts. Each of them can break. 
JPT aims to be robust and give insights despite imperfections of real world systems.

We understand reliability as the probability of successfully running end-to-end.

JPT focuses on repeatable, predictable builds. It provides the hardware in AWS rather than rely on local hardware
It always measures the baseline to avoid hidden long-term changes. It handles network problems. It includes
self-healing mechanisms. It creates environments from scratch. It expects and measures problems within Jira.
JPT is continuously tested, so we can catch breakages and flakiness.

#### Ease

Performance testing involves multiple steps. Each of them requires effort. Any manual step can lead to a mistake
and restarting the process. It's hard work.
JPT aims to make it easy.

We understand ease as the quality of yielding value while minimizing need for human intervention.

JPT is built on automation. It encompasses end-to-end test scenarios. It implements tasks, which are usually left
for humans, e.g. analysis and judgement calls. It transparently sets up most of its runtime dependencies.
It reduces the entry threshold by minimizing the environment and infrastructure requirements.

#### Flexibility

Performance testing can take multiple forms with many methods, flows, variables and parameters.
JPT aims to be flexible and give freedom to choose the general structure and fine-tune low-level details.

We understand flexibility as the ease of change in reaction to new goals.

JPT uses a code-first approach, which allows users to construct new scenarios, extend any capability, reshape existing
constructs. It provides building blocks which can be assembled in different fashions. It provides multiple examples
of different assemblies of the building blocks. It provides multiple Jira datasets out of the box. It facilitates
expanding automation of virtual Jira users. Access to its code allows to change even fundamental design decisions,
e.g. one could abandon the approach of simulating real users and switch to HTTP-first world.
